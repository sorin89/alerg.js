/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.bootstrap.html
 */

module.exports.bootstrap = function(cb) {
  sails.services.passport.loadStrategies();
  sails.moment = require('moment');
  sails.CronJob = require('cron').CronJob;
  
  sails.nodemailer = require('nodemailer');
  sails.transporter = sails.nodemailer.createTransport({
    host: 'smtp.mandrillapp.com',
    port: 587,
    auth: {
        user: 'sorin.cristea@me.com',
        pass: 'AN5Kb9gShOW5IJGAuKRmSA'
    }
  });

  sails.resulttoken = 'blablabla';


 /* //creeaza un cronjob:
    new sails.CronJob('20 * * * * *', function(){
    console.log('fiecare minut la secunda 20');
    }, null, true, "America/Los_Angeles"); */

  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  cb();
};
