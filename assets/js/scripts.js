function toggler(divId) {
    $("#" + divId).css('visibility','visible').show();
}

$(function() {
      $( "#birthdate" ).datepicker({
        changeYear: true,
        changeMonth: true,
      //  changeMonth: true,
        maxDate: new Date(2014, 12 - 1, 31),
        yearRange: "1900:2014",
        dateFormat: "yy-mm-dd",
        firstDay: 1,
        monthNames: [ "Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie" ],
        dayNamesMin: [ "Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ" ]
        });
      });

