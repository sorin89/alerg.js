/**
 * ParticipationController
 *
 * @description :: Server-side logic for managing participations
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	answer: function (req, res, next) {
        var answer = req.param('answer');
        if (answer == 'da') answer = true;
        if (answer == 'nu') answer = false;
        Race.current(sails.moment()._d, function(err, current) {if(err) return next(err); nextrace = current.id;});
        Participation.findOne({user:req.user.id, race: nextrace})
            .exec(function findParticipation(err, found) { 

            if (found)
                Participation.update({user:req.user.id, race: nextrace},{answer: answer})
                    .exec(function updateParticipation(err, updated){ 
                        if (err) return next(err);
                        req.session.answer = answer;
                        if(answer)
                            req.flash('success', 'Success.ParticipationUpdateYes');
                        else
                            req.flash('success', 'Success.ParticipationUpdateNo');
                        res.redirect('/');
                    });
            else
                Participation.create({user:req.user.id, race: nextrace, answer: answer})
                    .exec(function createParticipation(err,created){
                        if (err) return next(err);
                        req.session.answered = true;
                        req.session.answer = created.answer;
                        if (created.answer)
                            req.flash('success', 'Success.userParticipate')
                        else
                            req.flash('success', 'Success.userNotParticipate')
                        res.redirect('/');
                    });

            });
    },
    list: function (req, res, next) {
        Race.current(sails.moment()._d, function(err, current) {
            if(err) return next(err); 
            nextrace = current.id;
            Participation.find({race: nextrace, answer: true})
                .populateAll()
                .exec(function findParticipants(err, participants) {
                    res.view({participants: participants});
                });
        });
    }
};

