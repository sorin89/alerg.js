/**
 * RacesController
 *
 * @description :: Server-side logic for managing races
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  
  rezultate: function (req, res, next) {
    Race.find({sort :'date DESC'})
        .populateAll()
        .exec(function FoundRaces (err, races){
    if (err) return next(err);
    res.view({races: races});
    })  
  }
 
 /* cursa: function (req, res, next) {
    var race = req.param('race');
    Race.findOne({id:race})
        .populate('results')
        .exec(function(err,races){
          res.view({races: races});
          console.log(races.toJSON());
        });
  }*/

};

