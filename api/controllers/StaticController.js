/**
 * StaticController
 *
 * @description :: Server-side logic for managing statics
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  info: function (req, res) {
    res.view();
  },
  traseu: function (req, res) {
    res.view();
  },
  contact: function (req, res) {
    res.view();
  },
  news: function (req, res) {
    res.view();
  },
  homepage: function (req, res, next) {

    Race.current(sails.moment().subtract(60, 'minutes').format(), function(err, current) {
      if(err) return next(err);
      sails.nextrace = current;
      res.view({errors: req.flash('error'), success: req.flash('success')});
    });
  }
};

