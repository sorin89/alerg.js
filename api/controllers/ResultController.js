/**
 * ResultController
 *
 * @description :: Server-side logic for managing results
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  
  cursa: function (req, res, next) {
    var race = req.param('race');
    Race.findOne()
        .populate('location')
        .where({id: race})
        .exec(function (err, therace) {
          if(err) return next(err);
          Result.find({sort :'position ASC'})
                .populateAll()
                .where({race: race})
                .exec(function (err, results) {
                  if(err) return next(err);
                  res.view ({results: results, race: therace });
                });
        }); 
  },

  addresult: function(req, res, next) {
    if (typeof req.query.nexmo_caller_id != "undefined")
      var msisdn = req.query.nexmo_caller_id.substring(1);
    else
      var msisdn = 'missing';
    var xml = require('xml');
    function vxmlresponse(caseresponse) {
      return [{ vxml: [ { _attr: { version: '2.1'} }, { form: [{block: [{prompt: [ { _attr: { 'xml:lang': 'ro-ro-female'} }, caseresponse ] }] }] } ] }];
    }

    
    //find user by phone number
    User.findOne()
          .where({tel: msisdn /*, tel_c: true*/})
          .exec(function (err, foundUser) {
            if (foundUser)
            //did he already send the result for this race?
            Result.findOne()
                  .where({user: foundUser.id, race: sails.nextrace.id})
                  .exec(function (err, foundResult) {

                    if (foundResult) {
                      //res.view({found: 'found!', nextrace: sails.nextrace.date});
                      caseresponse = 'Salut, ' + foundUser.firstname + '. Timpul tău a fost deja înregistrat pentru această cursă!';
                      res.set('Content-Type', 'text/xml');
                      res.send(xml(vxmlresponse(caseresponse), true));
                    }
                    else {
                      startcount = sails.moment(sails.nextrace.date).add(12, 'minutes');
                      endcount = sails.moment(sails.nextrace.date).add(60, 'minutes');
                      now = sails.moment();
                    
                      if (startcount > now || endcount < now)
                      {
                        //res.view({found: 'data nu este intre parametri!', nextrace: sails.nextrace.date});
                        caseresponse = 'Salut, ' + foundUser.firstname + '. Sună la acest număr când participi la o cursă, după ce treci de linia de finish. Mulțumim!';
                        res.set('Content-Type', 'text/xml');
                        res.send(xml(vxmlresponse(caseresponse), true));
                      }
                      else
                      {
                        //how many have already finished?
                        Result.count()
                              .where({race: sails.nextrace.id})
                              .exec(function (err, count){ 
                                count++;
                                //add the result of this user to the current race
                                Result.create({race: sails.nextrace.id, position: count, user: foundUser.id, time: sails.moment().format()})
                                      .exec(function (err, created) {
                                        if(err) return next(err);
                                        //res.view({found: 'not found, l-am creat acum', nextrace: sails.nextrace.date});
                                        caseresponse = 'Felicitări, ' 
                                                        + foundUser.firstname 
                                                        + '. Ai ieșit pe locul ' 
                                                        + count 
                                                        + '. Ai avut un timp de ' 
                                                        + sails.moment.utc(sails.moment().diff(sails.moment(sails.nextrace.date))).format("mm") 
                                                        + ' minute și'
                                                        + sails.moment.utc(sails.moment().diff(sails.moment(sails.nextrace.date))).format("ss")
                                                        + ' secunde.' ;
                                        res.set('Content-Type', 'text/xml');
                                        res.send(xml(vxmlresponse(caseresponse), true));
                                      });
                              });
                      }
                    }

                  });
            else {
              //res.view({found: 'utilizatorul nu există în baza de date', nextrace: sails.nextrace.date});
              caseresponse = 'Salut. Nu am putut găsi numărul tău în baza de date. Creează-ți un cont pe www punct Alerg puncteu , și asigură-te că nu ne suni cu număr ascuns.';
              res.set('Content-Type', 'text/xml');
              res.send(xml(vxmlresponse(caseresponse), true));
            }
          });
  }


};

