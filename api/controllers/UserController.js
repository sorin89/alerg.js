/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  profil: function (req, res) {
    if(!req.user.email_c) req.flash('error', 'Error.EmailNotConfirmed');
    if(req.user.email_c && !req.user.tel_c) req.flash('error', 'Error.TelNotConfirmed');
    User.hasAnswered(req.user.id, function(err, answer) {

      Result.find()
            .where({user: req.user.id})
            .populate('race')
            .exec(function UserResults(err, results) {
              if(err) return next(err);
              req.session.answered = answer.q;
              if (answer) req.session.answer = answer.ans;
              res.view({ results: results, errors: req.flash('error'), success: req.flash('success')});
      });
    });


  },
  userprofil: function (req, res, next) {
    var username = req.param('username');
    User.findOne()
          .where({username: username})
          .populate('results')
          .exec(function getUser(err, utilizator) {
            if(err) return next(err);

            Result.find()
                  .where({user: utilizator.id})
                  .populate('race')
                  .exec(function getResults(err, results) {
                    if(utilizator)
                      res.view({utilizator: utilizator, results: results, errors: req.flash('error'), success: req.flash('success')});
                    else {
                      req.flash('error', 'Error.Usernotfound');
                      res.redirect('/');
                    }
                    
                  });
    });
  },
  admin: function (req, res) {
      res.view();
  },
  activate: function (req, res, next) {
    var user = req.param('user');
    var token = req.param('token');
    User.count().where({username:user, token:token}).exec(function counted(err, found){
      if (err) return next(err);
      if(found) {
         User.update({username:user, token:token},{email_c:'true'})
            .exec(function updates(err, updated){
            if (err) return next(err);
            req.flash('success', 'Success.confirmedEmail');
            res.redirect('/profil');
            });
      }
      else {
        req.flash('error', 'Error.unconfirmedEmail');
        res.redirect('/');
      }
    });      
  },
  reactivate: function (req, res, next) {
    mailer.sendActivation({email: req.user.email, username: req.user.username, token: req.user.token});
    req.flash('success', 'Success.resentActivation');
    res.redirect('/profil');
  },
  reset: function (req, res, next) {
    var id = req.param('identifier');
    var newtoken = Math.floor((Math.random() * 1000000) + 54);
    User.count().where({or: [{username: id}, {email: id}]})
        .exec(function(err, found){
        
            if(found) {
                User.findOne().where({or: [{username: id}, {email: id}]})
                    .exec(function(err, user) {
                        if (err) return next(err);
                        user.token = newtoken;
                        User.update({or: [{username: id}, {email: id}]},{token: newtoken})
                            .exec(function updates(err, updated){
                                if(err) return next(err);
                                req.flash('success', 'Success.sentReset');
                                res.redirect('/');
                                mailer.sendReset({email: user.email, 
                                                  username: user.username, 
                                                  token: user.token});                             
                            });
                    });    
            }
            else {
              req.flash('error', 'Error.notReset');
              res.redirect('/login');
            }

        });
  },
  resetpass: function (req, res, next) {
    var username = req.param('username');
    var token = req.param('token');
    User.count().where({username:username, token:token}).exec(function counted(err, found){
      if (err) return next(err); 
      if(found) {
        res.view({username: username, token: token, errors: req.flash('error')});
      }
      else {
        req.flash('error', 'Error.invalidResetToken');
        res.redirect('/');
      }
    });
  },
  resetdo: function (req, res, next) {
    var username = req.param('username');
    var token = req.param('token');
    var pass1 = req.param('pass1');
    var pass2 = req.param('pass2');
    User.count().where({username:username, token:token}).exec(function counted(err, found){
      if (err) return next(err); 
      if(found) {
        if(pass1 && pass1 == pass2 && pass1.length >= 8) {
          User.findOne().where({username:username})
              .exec(function(err, user){
                Passport.update({user: user.id},{password: pass1})
                        .exec(function updates(err, updated){
                          if(err) return next(err);
                          req.flash('success', 'Success.changedPass');
                          res.redirect('/');
                        });
              });

        }
        else {
          req.flash('error', 'Error.diffPassOrSmall');
          res.view('user/resetpass', {username: username, token: token, errors: req.flash('error')});
        }
      }
      else {
        req.flash('error', 'Error.invalidResetToken');
        res.redirect('/');
      }
    });
  },
  confirmaretel: function (req, res, next) {
    req.session.newtoken = Math.floor((Math.random() * 1000000) + 54);

                        User.update({username: req.user.username},{token: req.session.newtoken})
                            .exec(function updates(err, updated){
                                if(err) return next(err);
                                req.session.message = 'Salut. Codul tau pentru Alerg.eu este ' + req.session.newtoken + ' .';
                                
                                var request = require('request');
                                request('https://rest.nexmo.com/sms/json?api_key=c4169d77&api_secret=3105791b&from=40371700077&to=4' + req.user.tel + '&text=' + req.session.message, function (error, response, body) {
                                  if (!error && response.statusCode == 200) {
                                    console.log(body) // Show the HTML for the Google homepage. 
                                  }
                                })

                                req.flash('success', 'Success.sentText');
                                res.redirect('/confirmtel');

                            });
  },  

  confirmtel: function (req, res, next) {
    res.view({success: req.flash('success'), errors: req.flash('error')});
  },
  confirmteldo: function(req, res, next) {
    var smscode = req.param('smscode');
    User.count().where({username:req.user.username, token:smscode}).exec(function counted(err, found){
      if (err) return next(err);
      if(found) {
         User.update({username:req.user.username, token:smscode},{tel_c:'true'})
            .exec(function updates(err, updated){
            if (err) return next(err);
            req.flash('success', 'Success.confirmedSms');
            res.redirect('/profil');
            });
      }
      else {
        req.flash('error', 'Error.unconfirmedSms');
        res.redirect('/confirmtel');
      }
    });
  },
  updateprofil: function(req, res, next) {
    var firstname = req.param('firstname');
    var lastname = req.param('lastname');
    var birthdate = req.param('birthdate');
    var sex = req.param('sex');
    var weight = req.param('weight');
    var height = req.param('height');
    User.update({id: req.user.id},{firstname: firstname,
                                   lastname: lastname,
                                   birthdate: birthdate,
                                   sex: sex,
                                   weight: weight,
                                   height: height})
        .exec(function updates(err, updated){
          if(err) return next(err);
          req.flash('success', 'Success.updatedProfile');
          res.redirect('/profil');                           
        });
  }

};

