/**
* Result.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  schema: true,
  attributes: {
    race : {model : 'race', required: true},
    position : {type: 'integer', required: true},
    user : {model : 'user', required: true},
    time : {type: 'datetime', required: true},
  }
};

