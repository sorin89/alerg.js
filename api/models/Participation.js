/**
* Participation.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  schema: true,
  attributes: {
    user: {model: 'user', required: true},
    race: {model: 'race', required: true},
    answer: {type: 'boolean', defaultsTo: '0'}
  }
};

