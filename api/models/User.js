var User = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    username: { type: 'string', unique: true },
    email: { type: 'email',  unique: true },
    passports: { collection: 'Passport', via: 'user' },
    results: { collection: 'result', via: 'user' },
    participations: {collection: 'participation', via: 'user'},
    admin: {type: 'boolean', defaultsTo: '0'},
    token: {type: 'string'},
    tel: {type: 'string', unique: true },
    firstname: {type: 'string'},
    lastname: {type: 'string'},
    birthdate: {type: 'date'},
    sex: {type: 'integer'},
    height: {type: 'integer'},
    weight: {type: 'float'},
    email_c: {type: 'boolean', defaultsTo: '0'},
    tel_c: {type: 'boolean', defaultsTo: '0'},

    ConfirmedEmail: function() {
      return !!this.email_c;
    },
    ConfirmedTel: function() {
      return !!this.tel_c;
    },
    isAdmin: function() {
      return !!this.admin;
    }
  },
    hasAnswered: function(user, cb) {
      Race.current(sails.moment()._d, function(err, current) {nextrace = current.id;
        Participation.findOne().where({user: user, race: nextrace}).exec(function (err, found) {
          if(err) return cb(err);
          answered = {};
          if(found) {
            answered.q = true;
            answered.ans = found.answer; 
          }
          else {
            answered.q = false;
          }
          return cb(null, answered);
        });
      });
    }

  
};

module.exports = User;
