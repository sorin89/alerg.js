/**
* Races.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  
  schema: true,

  attributes: {
    date : {type: 'datetime', required: true},
    location : {model : 'location', required: true},
    results : {collection: 'result', via: 'race'},
    participations : {collection: 'participation', via: 'race'},
	},

  current: function(today, cb) {
    Race.findOne({where: { date: { '>=': today }}, sort: 'date ASC'})
        .exec(function (err, found) {
      if(err) return cb(err);
      return cb(null, found);
    });
  }
  
};
