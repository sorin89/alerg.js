module.exports = {
    sendActivation: function (opt) {
        var mailOptions = {
            from: 'Alerg.eu <info@alerg.eu>',
            to: opt.email, // list of receivers 
            subject: opt.username + ', confirma-ti adresa de email',
            text: 'Salut,' + opt.username + '.\n Confirma-ti adresa de e-mail apasand pe acest link: \n http://alerg.eu/activeaza/' + opt.username + '/' + opt.token + '\n Cu drag, \n Echipa Alerg.eu',            
            html: 'Salut,' + opt.username + '.<br/>Confirma-ti adresa de e-mail apasand pe linkul de mai jos: <br/><a href=\"http://alerg.eu/activeaza/' + opt.username + '/' + opt.token + '\">http://alerg.eu/activeaza/'+ opt.username + '/'+ opt.token +'</a><br/> Cu drag, <br/> Echipa Alerg.eu' // html body 
        };
        
        sails.transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log(' Activation Message sent: ' + info.response);
            }
        });
    },
    sendReset: function (opt) {
        var mailOptions = {
            from: 'Alerg.eu <info@alerg.eu>',
            to: opt.email, // list of receivers 
            subject: 'Cerere de resetare a parolei',
            text: 'Salut,' + opt.username + '.\n Du-te la acest link pentru a iti reseta parola: \n http://alerg.eu/resetpass/' + opt.username + '/' + opt.token + '\n Cu drag, \n Echipa Alerg.eu',            
            html: 'Salut,' + opt.username + '.<br/>Du-te la acest link pentru a iti reseta parola: <br/><a href=\"http://alerg.eu/resetpass/' + opt.username + '/' + opt.token + '\">http://alerg.eu/resetpass/'+ opt.username + '/'+ opt.token +'</a><br/> Cu drag, <br/> Echipa Alerg.eu' // html body 
        };
        
        sails.transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }else{
                console.log('Pass Reset Message sent: ' + info.response);
            }
        });
    }
};
