var validator = require('validator');

/**
 * Local Authentication Protocol
 *
 * The most widely used way for websites to authenticate users is via a username
 * and/or email as well as a password. This module provides functions both for
 * registering entirely new users, assigning passwords to already registered
 * users and validating login requesting.
 *
 * For more information on local authentication in Passport.js, check out:
 * http://passportjs.org/guide/username-password/
 */

/**
 * Register a new user
 *
 * This method creates a new user from a specified email, username and password
 * and assign the newly created user a local Passport.
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
exports.register = function (req, res, next) {
  var email = req.param('email')
    , username = req.param('username')
    , password = req.param('password')
    , tel = req.param('tel')
    , firstname = req.param('firstname')
    , lastname = req.param('lastname')
    , birthdate = req.param('birthdate')
    , sex = req.param('sex')
    , height = req.param('height')
    , weight = req.param('weight')
    , token = Math.floor((Math.random() * 1000000) + 54);

  if (!email) {
    req.flash('error', 'Error.Passport.Email.Missing');
    return next(new Error('No email was entered.'));
  }
  if (!username) {
    req.flash('error', 'Error.Passport.Username.Missing');
    return next(new Error('No username was entered.'));
  }
  if (!password) {
    req.flash('error', 'Error.Passport.Password.Missing');
    return next(new Error('No password was entered.'));
  }
  if (!tel) {
    req.flash('error', 'Error.Passport.Tel.Missing');
    return next(new Error('No telephone was entered.'));
  }
  if (!firstname) {
    req.flash('error', 'Error.Passport.Firstname.Missing');
    return next(new Error('No first name was entered.'));
  }
  if (!lastname) {
    req.flash('error', 'Error.Passport.Lastname.Missing');
    return next(new Error('No last name was entered.'));
  }
  if (!birthdate) {
    req.flash('error', 'Error.Passport.Birthdate.Missing');
    return next(new Error('No birthdate was entered.'));
  }
  if (!sex) {
    req.flash('error', 'Error.Passport.Sex.Missing');
    return next(new Error('No birthdate was entered.'));
  }
  if (!height) {
    req.flash('error', 'Error.Passport.Height.Missing');
    return next(new Error('No height was entered.'));
  }
  if (!weight) {
    req.flash('error', 'Error.Passport.Weight.Missing');
    return next(new Error('No weight was entered.'));
  }
  if (!validator.isNumeric(tel) || !validator.isLength(tel, 10, 10) || !validator.matches(tel, '^[0][7]')) {
    req.flash('error', 'Error.Passport.Tel.Incorrect');
    return next(new Error('Phone number incorrect.'));    
  }



  User.create({
    username : username
  , email : email
  , token : token
  , tel : tel 
  , firstname : firstname
  , lastname : lastname
  , birthdate : birthdate
  , sex : sex
  , height : height
  , weight : weight
  }, function (err, user) {
    if (err) {
      if (err.code === 'E_VALIDATION') {
        if (err.invalidAttributes.email) {
          req.flash('error', 'Error.Passport.Email.Exists');
        }
        if (err.invalidAttributes.username) {
          req.flash('error', 'Error.Passport.Username.Exists');
        }
        if (err.invalidAttributes.tel) {
          req.flash('error', 'Error.Passport.Tel.Exists');
        }
        if (err.invalidAttributes.birthdate) {
          req.flash('error', 'Error.Passport.Birthdate.Invalid');
        }
        if (err.invalidAttributes.height) {
          req.flash('error', 'Error.Passport.Height.Invalid');
        }
        if (err.invalidAttributes.weight) {
          req.flash('error', 'Error.Passport.Weight.Invalid');
        }
      }

      return next(err);
    }

    Passport.create({
      protocol : 'local'
    , password : password
    , user     : user.id
    }, function (err, passport) {
      if (err) {
        if (err.code === 'E_VALIDATION') {
          req.flash('error', 'Error.Passport.Password.Invalid');
        }

        return user.destroy(function (destroyErr) {
          next(destroyErr || err);
        });
      }
    
    mailer.sendActivation({email: email, username: username, token: token});

      next(null, user);
    });
  });
};

/**
 * Assign local Passport to user
 *
 * This function can be used to assign a local Passport to a user who doens't
 * have one already. This would be the case if the user registered using a
 * third-party service and therefore never set a password.
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
exports.connect = function (req, res, next) {
  var user     = req.user
    , password = req.param('password');

  Passport.findOne({
    protocol : 'local'
  , user     : user.id
  }, function (err, passport) {
    if (err) {
      return next(err);
    }

    if (!passport) {
      Passport.create({
        protocol : 'local'
      , password : password
      , user     : user.id
      }, function (err, passport) {
        next(err, user);
      });
    }
    else {
      next(null, user);
    }
  });
};

/**
 * Validate a login request
 *
 * Looks up a user using the supplied identifier (email or username) and then
 * attempts to find a local Passport associated with the user. If a Passport is
 * found, its password is checked against the password supplied in the form.
 *
 * @param {Object}   req
 * @param {string}   identifier
 * @param {string}   password
 * @param {Function} next
 */
exports.login = function (req, identifier, password, next) {
  var isEmail = validator.isEmail(identifier)
    , query   = {};

  if (isEmail) {
    query.email = identifier;
  }
  else {
    query.username = identifier;
  }

  User.findOne(query, function (err, user) {
    if (err) {
      return next(err);
    }

    if (!user) {
      if (isEmail) {
        req.flash('error', 'Error.Passport.Email.NotFound');
      } else {
        req.flash('error', 'Error.Passport.Username.NotFound');
      }

      return next(null, false);
    }

    Passport.findOne({
      protocol : 'local'
    , user     : user.id
    }, function (err, passport) {
      if (passport) {
        passport.validatePassword(password, function (err, res) {
          if (err) {
            return next(err);
          }

          if (!res) {
            req.flash('error', 'Error.Passport.Password.Wrong');
            return next(null, false);
          } else {
            return next(null, user);
          }
        });
      }
      else {
        req.flash('error', 'Error.Passport.Password.NotSet');
        return next(null, false);
      }
    });
  });
};
